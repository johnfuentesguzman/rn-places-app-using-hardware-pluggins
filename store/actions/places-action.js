import * as FileSystem from 'expo-file-system'; // getting all filesystem device handling 

export const ADD_PLACE = 'ADD_PLACE';
export const FETCH_PLACES = 'FETCH_PLACES';

import { insertPlace, fetchPlaces } from '../../helpers/db';

export const addPlace = (title, image) => {
  return async dispatch => { // remmeber that redux thunk need return a dispatch function mandatory 
    const fileName = image.split('/').pop(); //pop  takes last element/index of array
    const newPath = FileSystem.documentDirectory + fileName; // documentDirectory main  app folder.. will persist untill the user unistall the app

    try {
      await FileSystem.moveAsync({ // saving in device  folder
        from: image,
        to: newPath
      });
      const dbResult = await insertPlace( // savinng in DB
        title,
        newPath,
        'Dummy address',
        15.6,
        12.3
      );
      console.log(dbResult);
      dispatch({ type: ADD_PLACE, placeData: { id: dbResult.insertId, title: title, image: newPath } });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};


export const loadPlaces = () => {
  return async dispatch => {
      try {
          const dbResult = await fetchPlaces();
          console.log(dbResult);
          dispatch({ type: FETCH_PLACES, places: dbResult.rows._array }); //_array This is the format/payload how sqlite saved data
      } catch (err) {
          throw err;
      }
  };
};