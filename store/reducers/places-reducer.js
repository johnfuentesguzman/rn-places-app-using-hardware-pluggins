import { ADD_PLACE, FETCH_PLACES } from "../actions/places-action";
import Place from '../../models/place';

const initiaState = {
    places: []
};

export default (state = initiaState, action) => {
    switch (action.type) {
        case FETCH_PLACES:
            return {
                places: action.places.map( // thus aarray will have all places stored
                    (pl) => new Place(pl.id.toString(), pl.title, pl.imageUri)
                )
            };
        case ADD_PLACE:
            const placeObj = new Place(action.placeData.id.toString(), action.placeData.title, action.placeData.image)
            return { ...state, places: state.places.concat(placeObj) }; // concat to join two or more arrays.

        default:
            return initiaState;
    }
}     