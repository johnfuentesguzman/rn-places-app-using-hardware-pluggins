import 'react-native-gesture-handler'; // must be ALWAYS FIRST
import React, {useState} from 'react';
import { Platform } from 'react-native'
import { NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as Font from 'expo-font';
import { AppLoading} from 'expo';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import { Provider }  from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxThunk  from 'redux-thunk';

import MapReducer from './store/reducers/map-reducer';
import PlaceReducer from './store/reducers/places-reducer';

import MapScreen from './screens/MapScreen';
import NewPlaceScreen from './screens/NewPlaceScreen';
import PlaceDetailScreen from './screens/PlaceDetailScreen';
import PlacesListScreen from './screens/PlacesListScreen';

import Colors from './constants/Colors';
import { navigationRef } from './service/navigation-service';
import { init } from './helpers/db';

init().then(
  () => {
    console.log('database sqlite initialized');
  })
  .catch((error) => {
      console.log('error initializing  ',  error); 
  })

const rootReducer = combineReducers({ 
  mapState: MapReducer,
  placesState: PlaceReducer
});

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(ReduxThunk),
  // other store enhancers if any
));

const fetchFonts=() => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'opens-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};

const Stack = createStackNavigator();

function stackScreens({ navigation }) {
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('tabPress', e => {
      e.preventDefault();
      let tabPressed = e.target.split("-")[0].toUpperCase();
      // I need to do this, because just setting the page as STACK navigitation , those page will have header
      switch (tabPressed) {
        case 'MAP':
          navigation.navigate('Map');
        break;
        case 'PLACESLIST':
          navigation.navigate('PlaceslistScreen');
        break;
      }
    });

    return unsubscribe;
  }, [navigation]);
  return (
    <Provider store={store}>
        <Stack.Navigator>
          <Stack.Screen name="MapScreen" component={MapScreen} ptions={{ title: 'Map' }}/>
          <Stack.Screen name="NewPlaceScreen" component={NewPlaceScreen} options={{ title: 'Add New Place' }}/>
          <Stack.Screen name="PlaceDetailScreen" component={PlaceDetailScreen} options={{ title: 'Place Details' }}/>
          <Stack.Screen name="PlaceslistScreen" component={PlacesListScreen} options={{ title: 'All Places' }}/>
        </Stack.Navigator>
    </Provider>
  );
}

const Tab = createBottomTabNavigator();

export const App = () => {
  const [fontsLoaded, setFontsLoaded] = useState(false);

  if(!fontsLoaded){
    return <AppLoading startAsync={fetchFonts} onFinish={()=> setFontsLoaded(true)}/>
  }

// navigationRef: set a ref to tell you that your app has finished mounting, and it gets all the navigation tree before to perform them
  return (
    <NavigationContainer  ref={ navigationRef } > 
      <Tab.Navigator
        initialRouteName="PlacelistScreen" // main screen
        screenOptions={({ route }) => ({
          headerStyle: {
            backgroundColor: Colors.headerBackGroundColor,
          },
          headerTintColor: Colors.headerTintColor,
          headerTitleStyle: {
            fontWeight: Colors.headerfontWeight,
          },
        })}
      >
        <Tab.Screen name="Map" component={stackScreens}  options={{
          tabBarLabel: 'Map',
          tabBarIcon:() => (
            <Ionicons name={Platform.OS === 'android' ? 'md-map' : 'ios-map'} size={28} color='black' />
          ),
        }} />
        <Tab.Screen name="Placeslist" component={stackScreens} options={{
          tabBarLabel: 'Places',
          tabBarIcon:() => (
            <Ionicons name={Platform.OS === 'android' ? 'md-ribbon' : 'ios-ribbon'} size={28} color='black' />
          )
        }}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
} 

export default App;