import React, { useEffect } from 'react';
import { View, Text, StyleSheet, Platform, FlatList } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import CustomHeaderButton from '../components/CustomHeaderButtons';
import PlaceItem from '../components/PlaceItem';
import * as placesActions from '../store/actions/places-action';

const PlacesListScreen = (props) => {
  const places = useSelector(state => state.placesState.places);
  const { navigation } = props;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(placesActions.loadPlaces());
  }, [dispatch]);


  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => null, // removing default back button
      headerRight: () => (
        <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
          <Item title='Add Place' iconName={Platform.OS === 'android' ? 'md-add' : 'ios-add'} onPress={() => { navigation.navigate('NewPlaceScreen') }} />
        </HeaderButtons>
      )
    });
  }, [])


  if (places && places.length === 0 ) {
    return (
      <View style={styles.centered}>
        <Text>No exist photos to show</Text>
      </View>
    );
  }

  return (
    <FlatList
      data={places}
      keyExtractor={item => item.id}
      renderItem={itemData => (
         // attention: the params for the Item render function  MUST BE SPLITED ONE BY ONE  
        <PlaceItem
          image={itemData.item.imageUri}
          title={itemData.item.title}
          address={'ciudad del bosque'}
          onSelect={() => {
            navigation.navigate('PlaceDetailScreen', {
              placeTitle: itemData.item.title,
              placeId: itemData.item.id
            });
          }}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  centered: { flex: 1, justifyContent: 'center', alignItems: 'center' }
});

export default PlacesListScreen;
