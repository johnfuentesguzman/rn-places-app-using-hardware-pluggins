import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import {
  ScrollView,
  View,
  Button,
  Text,
  TextInput,
  StyleSheet
} from 'react-native';

import Colors from '../constants/Colors';
import * as  placeActions from '../store/actions/places-action';
import ImagePicker from '../components/ImagePicker';
import LocationPicker from '../components/LocationPicker';

const NewPlaceScreen = props => {
  const [titleValue, setTitleValue] = useState('');
  const [imageFromDevice, setImageFromDevice] = useState(null);
  const dispatch = useDispatch();

  const titleChangeHandler = text => {
    if(text.length === 0){
      return;
    }
    setTitleValue(text);
  };

  const savePlaceHandler = () => {
    dispatch(placeActions.addPlace(titleValue, imageFromDevice));
    props.navigation.goBack();
  };

  const imageTakenHandler = (imagePath) => {
    setImageFromDevice(imagePath);
  };
  
  return (
    <ScrollView>
      <View style={styles.form}>
        <Text style={styles.label}>Title</Text>
        <TextInput
          style={styles.textInput}
          onChangeText={titleChangeHandler}
          value={titleValue}
        />
        <ImagePicker onImageTaken={imageTakenHandler} onSaveImage={savePlaceHandler}/>
        <LocationPicker/>

      </View>
    </ScrollView>
  );
};

NewPlaceScreen.navigationOptions = {
  headerTitle: 'Add Place'
};

const styles = StyleSheet.create({
  form: {
    margin: 30 // all directions
  },
  label: {
    fontSize: 18,
    marginBottom: 15
  },
  textInput: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    marginBottom: 15,
    paddingVertical: 4, // up and bottom
    paddingHorizontal: 2
  }
});

export default NewPlaceScreen;
