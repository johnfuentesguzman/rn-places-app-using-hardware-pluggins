import * as React from 'react';
import {  Alert } from 'react-native';

export const navigationRef = React.createRef();  // navigationRef: set a ref to tell you that your app has finished mounting, and it gets all the navigation tree before to perform them

export function navigateToLogout (routeName, params) {
  Alert.alert('You session has expired!', 'Please login again', [{ text: 'Okay' }]);
  navigationRef.current?.navigate(routeName, params);
};

// based on https://reactnavigation.org/docs/navigating-without-navigation-prop/  and https://medium.com/@dariaruckaolszaska/navigate-from-your-redux-actions-with-react-navigation-in-your-react-native-app-d3bf1fbd4c08