import React from 'react';
import { View, Text, Image, StyleSheet, Platform, TouchableOpacity, TouchableNativeFeedback } from 'react-native';
import Colors from '../constants/Colors';

const PlaceItem = (props) => {
  let TouchableComp = TouchableOpacity;
  if (Platform.OS === 'android' && Platform.Version >= 21) {
    TouchableComp = TouchableNativeFeedback;
  }

  return (
    <TouchableNativeFeedback onPress={props.onSelect} style={styles.placeItem} useForeGround>
      <View style={styles.screen}>{/* this View is due to for android we have a rule that we need to have all the content inside touchable comp into a single view */}

        <Image style={styles.image} source={{ uri: props.image }} />
        <View style={styles.infoContainer}>
          <Text style={styles.title}>{props.title}</Text>
          <Text style={styles.address}>{props.address}</Text>
        </View>
      </View>

    </TouchableNativeFeedback>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    flexDirection: 'row',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    marginVertical: 10,
    overflow: 'hidden'
  },
  placeItem: {
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center'
  },
  image: {
    width: 70, // image into circle markup
    height: 70, // image into circle markup
    borderRadius: 35,
    backgroundColor: '#ccc',
    borderColor: Colors.primary,
    borderWidth: 1
  },
  infoContainer: {
    marginLeft: 25,
    width: 250,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  title: {
    color: 'black',
    fontSize: 18,
    marginBottom: 5
  },
  address: {
    color: '#666',
    fontSize: 16
  }
});

export default PlaceItem;
